
import org.junit.jupiter.api.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class Activity3Tests
{
    /*
     * Test website: http://www.mcdonalds.ca
     * Ids:
     *  - Modal: a.exit
     *  - Name of section: .click-before-outline
     *  - First name: #firstname2
     *  - E-mail address: #email2
     *  - Postal Code: #postalcode2
     *  - Subscribe btn: #g-recaptcha-btn-2
     */

	// -----------------------------------
	// Configuration variables
	// -----------------------------------
	// Location of chromedriver file
	final String CHROMEDRIVER_LOCATION = "/Users/merendaz/Downloads/chromedriver";
	// Website we want to test
	final String URL_TO_TEST = "http://www.mcdonalds.ca";
			
	// -----------------------------------
	// Global driver variables
	// -----------------------------------
	WebDriver driver;

    @BeforeEach
    public void setUp() throws Exception
    {
    	// -------------------------------------------------
    	//Here goes the code to be executed before each Test
    	// -------------------------------------------------
		// 1. Selenium setup
		System.setProperty("webdriver.chrome.driver",CHROMEDRIVER_LOCATION);
        driver = new ChromeDriver();
		// 2. go to website
		driver.get(URL_TO_TEST);
		//3. Waiting a second for the modal to open and closing it after a second
        Thread.sleep(1000);
        WebElement closeModal = driver.findElement(By.cssSelector("a.exit"));
        closeModal.click();
        Thread.sleep(1000);
    }

    @AfterEach
    public void tearDown() throws Exception
    {
    	// ------------------------------------------------
    	//Here goes the code to be executed after each Test
    	// ------------------------------------------------
    	//1. Pause for 10 seconds before closing the browser
        Thread.sleep(10000);
        driver.close();
    }

    @Test
    public void TestTC1() throws InterruptedException
    {
    	// --------------------------------------------
    	// TC1:  Title of the subscription feature is 
    	// “Subscribe to my Mcd’s”
    	// --------------------------------------------     	
        WebElement fullTitleBox = driver.findElement(By.cssSelector(".click-before-outline"));
        WebElement registerTitleBox = driver.findElement(By.cssSelector(".click-before-outline>sup"));
        assertEquals("Subscribe to My McD’s", fullTitleBox.getText().replace(registerTitleBox.getText(), ""));
    }

    @Test
    public void TestTC2() throws InterruptedException
    {
    	// -------------------------------------------------
    	// TC2:  Email signup - happy path
    	// -------------------------------------------------
    	WebElement usernameBox = driver.findElement(By.id("firstname2"));
        usernameBox.sendKeys("James Smith");
        Thread.sleep(500);
        WebElement passwordBox = driver.findElement(By.id("email2"));
        passwordBox.sendKeys("james@user.com");
        Thread.sleep(500);
        WebElement postalCodeBox = driver.findElement(By.id("postalcode2"));
        postalCodeBox.sendKeys("M5S");
        Thread.sleep(500);
        WebElement subscribeBtn = driver.findElement(By.id("g-recaptcha-btn-2"));
        subscribeBtn.click();
        
        readCaptcha();
    }

    @Test
    public void TestTC3() throws InterruptedException
    {
    	// --------------------------------------------
    	// TC3: Email signup -  negative case
    	// --------------------------------------------    	
        WebElement subscribeBtn = driver.findElement(By.id("g-recaptcha-btn-2"));
        subscribeBtn.click();
        Thread.sleep(1000);
        
        readCaptcha();
    }
    
    public void readCaptcha() throws InterruptedException {
        Thread.sleep(2000);
        WebElement frameCaptcha = driver.findElement(By.xpath(".//iframe[@title='recaptcha challenge']"));
        driver.switchTo().frame(frameCaptcha);
        driver.findElement(By.xpath(".//*[@id='recaptcha-verify-button']")).click();
    }

}